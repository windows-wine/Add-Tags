AddTags is a small application designed to help in formatting text prior to posting to web sites, blogs, forums, etc.
It is written in AutoHotkey (an interpreted language, provided free, courtesy of Chris Mallett). Source code will be provided on request.
For now, the only accepted tags are those pertaining to WordPress blogs, although most of them are common HTML tags,
usable in many other places.

Warning: this application works by manipulating Clipboard contents.

After launching the application, it can be brought up with the default hotkey Ctrl+Shift+T. This hotkey can be changed in Settings.
When the hotkey is pressed, the application copies the selected text (if any) from the currently focused window into its Edit window.
The text in the Edit window can subsequently be partly, completely or not at all selected, to apply chosen formatting to the selection.
If nothing is selected, the chosen formatting will be applied to the whole chunk of text in the Edit window.

Currently there is no Undo function, so be careful what you select and what formatting you apply, otherwise you may have to correct manually.

The formatting buttons to the left are efficient both on your own blog and in your replies to other people's blogs (except maybe for 'Image').
The formatting buttons to the right are only efficient on your own blog (both in main post and your replies to commenters).

A second set of smileys has been added, starting with version 2.1.0.0. The large smileys are only available on private blogs running the MonaLisa plug-in.
Most smileys should work on both own blog and replies to others' blogs; please note however, that some owners may have disabled emoticon display
and as such, no smiley images will be displayed on their blogs. Nonetheless, in such case the respective codes will be displayed.

Starting with version 3.0, there is a Unicode version available, compiled with AHK_L 1.1.x.x which is supposed to run under x64 systems.
Please test and report if you encounter any issues, apart from the known bugs mentioned below. Win9x users should run the ANSI version.
_______________________________________________________________________

			QUICK HELP
_______________________________________________________________________
1. Click inside the browser's post/reply rectangle to bring the caret focus.

2a.
- type your full text
- use mouse to select the desired word/phrase
- press %dhk1% to show this app's window
- apply the desired formatting (multiple subsequent formatting allowed).
- SEND and repeat from selection as necessary

2b.
- press the chosen hotkey (default is CTRL+SHIFT+T) to show this app's window
- type your full text in the text field
- select a word/phrase and apply formatting
 - For subsequent formatting, select again

3. Press the SEND button to send the formatted text to the browser

4. Check the text and correct if necessary, then post the article/reply as usual
_______________________________________________________________________
URL formatting:
- type/paste URL address in the first Edit field at the top (where it says 'enter URL here')
- optionally, type an alternative text that will be shown as tooltip, in the second Edit field (where it says 'enter alternate text here')
- the bottom Edit window may contain either the same URL address or a text that will be inserted at the cursor position if nothing was previously selected

Example: if you typed in a post/reply "See this webpage here" and want 'here' to act as a link to a webpage, select the word 'here', bring up the application
by pressing the hotkey and fill in the two edit fields at the top, then click 'Send'.

Image formatting:
- type/paste (or select before pressing hotkey) the image URL in the Edit window at the bottom
- type/paste the same URL in the first Edit field at the top; alternatively you can type/paste a different URL if you want a certain page to open when clicking the image
- optionally type a description for the image in the second Edit field at the top

Quote:
- type commenter's name in the URL field (optional)
- type comment number in the [alternate text] field (you can find the number by hovering the post link on the web page)
- the bottom Edit window should contain the quoted text

Abbreviation:
- the bottom Edit window contains the abbreviation of a word or expression (i.e. extm.)
- the second Edit field at the top should contain the complete form of the word or expression (i.e. extreme)

Acronym:
- the bottom Edit window contains the acronym of a word or expression (i.e. 'a.s.a.p.')
- the second Edit field at the top should contain the complete form of the word or expression (i.e. 'as soon as possible')

Horizontal rule (HR):
- the second Edit field at the top may contain a custom height value between 1 and 10 pixels. Otherwise the default value will be used.
_______________________________________________________________________
Certain WordPress themes and also certain browsers may not recognize the new HTML tags ( <strong> <em> ),
while others may not recognize the older ones ( <b> <i> ). To assist in the issue, either of the styles can be selected
by (not) holding CTRL while clicking the respective button ( [B] [I] [U] [S]). Tooltip over button will show current style.
Alternative styles are also available for List (ordered/unordered) and Picture (border/no border).
_______________________________________________________________________
- Holding down Ctrl while selecting Yahoo smileys will produce the character codes (used in the messenger application) instead of image URL.
- Holding down Shift while selecting smileys in either group will keep the smiley window open to allow subsequent selections.Close it with Esc(ape).
- There is a special hotkey CTRL+SHIFT+V that can paste a string from clipboard to the currently focused window; useful with copy-protected sites where Paste isn't allowed.
Same function as the 'Send 2' button (in v4.3.1.0 and later).
_______________________________________________________________________
Known bugs:
- when UTF-16 text is selected, the Romanian diacritics ş/Ş and ţ/Ţ are being decomposed in the bottom Edit field as s, (S,) or t, (T,).
Fixing this issue will require a high amount of work so it may take a while. My apologies for the inconvenience! Please fix manually.

Enjoy! :-)

© Drugwash, January 2012-June 2015

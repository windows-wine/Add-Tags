; helper functions v1.3
;================================================================
EditGetFirstLine(hEdit)
;================================================================
{
Global Ptr, AW
return DllCall("SendMessage" AW
		, Ptr		, hEdit
		, "UInt"	, 0xCE			; EM_GETFIRSTVISIBLELINE
		, "UInt"	, 0
		, "UInt"	, 0)
}
;================================================================
GetHdrH(hList)
;================================================================
{
Global PtrSz, Ptr, AW
hhdr := DllCall("SendMessage" AW
		, Ptr		, hList
		, "UInt"	, 0x101F			; LVM_GETHEADER
		, "UInt"	, 0
		, "UInt"	, 0
		, Ptr)
VarSetCapacity(HDLAYOUT, 2*PtrSz, 0)
VarSetCapacity(RECT, 16, 0)
VarSetCapacity(WP, 20+2*PtrSz, 0)	; WINDOWPOS struct
NumPut(&RECT, HDLAYOUT, 0, Ptr)
NumPut(&WP, HDLAYOUT, PtrSz, Ptr)
DllCall("SendMessage" AW
		, Ptr		, hhdr
		, "UInt"	, 0x1205			; HDM_LAYOUT
		, "UInt"	, 0
		, Ptr		, &HDLAYOUT)
hy := NumGet(RECT, 4, "Int")
VarSetCapacity(HDLAYOUT, 0)
VarSetCapacity(RECT, 0)
VarSetCapacity(WP, 0)
return hy
}
;================================================================
EditSetRect(hEd, par="")
;================================================================
{
Global Ptr, AW
Static p="LTRB", RECT
if !par
	return DllCall("SendMessage" AW
			, Ptr		, hEd
			, "UInt"	, 0xB3		; EM_SETRECT
			, "UInt"	, 0
			, "UInt"	, 0)
VarSetCapacity(RECT, 16, 0)
WinGetPos,,, w, h, ahk_id %hEd%
x := y := "0"
Loop, Parse, par, %A_Space%
	{
	StringLeft, i, A_LoopField, 1
	StringTrimLeft, v, A_LoopField, 1
	x += v*(i="L")
	y += v*(i="T")
	w -= v*(i="R")
	h -= v*(i="B")
	}
NumPut(x, RECT, 0, "Int")
NumPut(y, RECT, 4, "Int")
NumPut(w, RECT, 8, "Int")	; take into account V scrollbar width?
NumPut(h, RECT, 12, "Int")
r := DllCall("SendMessage" AW
	, Ptr		, hEd
	, "UInt"	, 0xB3				; EM_SETRECT (don't use EM_SETRECTNP=0xB4)
	, "UInt"	, 0
	, Ptr		, &RECT)
VarSetCapacity(RECT, 0)
}
;================================================================
SyncScroll(hwnd, msg, wP, lP)
;================================================================
{
Critical
Global hEdit, hLV2, WProc, AW, Ptr
/*
if (msg=0x115)	; WM_VSCROLL
	if (hwnd=hEdit)
		{
GuiControl,, Static9, wP=%wP% lP=%lP% hwnd=%hwnd% hEdit=%hEdit% hLV2=%hLV2%
		DllCall("SendMessage" AW
			, Ptr		, hLV2
			, "UInt"	, 0x115		; WM_VSCROLL
			, "UInt"	, wP			; SB_LINEDOWN
			, "UInt"	, 0)
		DllCall("SendMessage" AW
			, Ptr		, hLV2
			, "UInt"	, 0xB5		; EM_SCROLL
			, "UInt"	, wP
			, "UInt"	, lP)
		return 1
		}
*/
return DllCall("CallWindowProc" AW
		, Ptr		, WProc
		, Ptr		, hwnd
		, "UInt"	, msg
		, Ptr		, wP
		, Ptr		, lP)
}

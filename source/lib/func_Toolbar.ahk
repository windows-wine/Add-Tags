;==VERSIONING==>1.1
;About:
;Compat:	B1.0.48.05 L1.1
;Depend:
;Rqrmts:
;Date:	2016.12.29 12:10
;Version:	1.1.3
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash
;==VERSIONING==<

; Button styles:  ======================================================
; *TBSTYLE_SEP=0x1 *TBSTYLE_CHECK=0x2 *TBSTYLE_GROUP=0x4 *TBSTYLE_DROPDOWN=0x8
; *TBSTYLE_AUTOSIZE=0x10 *TBSTYLE_NOPREFIX=0x20 *BTNS_SHOWTEXT=0x40
; *BTNS_WHOLEDROPDOWN=0x80
; Toolbar styles: ======================================================
; TBSTYLE_TOOLTIPS=0x100 TBSTYLE_WRAPABLE=0x200 TBSTYLE_ALTDRAG=0x400 TBSTYLE_FLAT=0x800
; TBSTYLE_LIST=0x1000 TBSTYLE_CUSTOMERASE=0x2000 TBSTYLE_REGISTERDROP=0x4000
; TBSTYLE_TRANSPARENT=0x8000
; Toolbar extended styles: ================================================
; TBSTYLE_EX_DRAWDDARROWS=0x1 TBSTYLE_EX_MIXEDBUTTONS=0x8
; TBSTYLE_EX_HIDECLIPPEDBUTTONS=0x10 TBSTYLE_EX_DOUBLEBUFFER=0x80
; Common controls styles (used with toolbar styles): =================================
; CCS_TOP=0x1 CCS_NOMOVEY=0x2 CCS_BOTTOM=0x3 CCS_NORESIZE=0x4
; CCS_NOPARENTALIGN=0x8 CCS_ADJUSTABLE=0x20 CCS_NODIVIDER=0x40 CCS_VERT=0x80
; CCS_LEFT=CCS_VERT|CCS_TOP CCS_RIGHT=CCS_VERT|CCS_BOTTOM
; CCS_NOMOVEX=CCS_VERT|CCS_NOMOVEY
;================================================================
TB_Create(g="1", c="0|0|0|0", style="0x9B15", exstyle="0x89")
;================================================================
{
Global PtrSz, Ptr, AW
Static idx=1
Static cs="xywh"
StringSplit, c, c, |
Loop, Parse, cs
	%A_LoopField% := c%A_Index% ? c%A_Index% : "0"
; get window handle
if !(hGUI := GetHwnd(g))
	{
	msgbox, Error in %A_ThisFunc%():`nInvalid GUI count: %g%.
	Return 0
	}
; 0x880E TBSTYLE_TRANSPARENT/FLAT/CCS_NOPARENTALIGN/NORESIZE/NOMOVEY
; default style: WS_CHILD/VISIBLE/CLIPSIBLINGS/CLIPCHILDREN/TABSTOP
if !hwnd := DllCall("CreateWindowEx" AW
		, "UInt", 0x80		; WS_EX_TOOLWINDOW
		, "Str", "ToolbarWindow32"
		, "Str", ""
		, "UInt", 0x56010000|style
		, "Int", x
		, "Int", y
		, "UInt", w
		, "UInt", h
		, Ptr, hGUI		; hWndParent
		, Ptr, 0			; hMenu
		, Ptr, 0			; hInstance
		, Ptr, 0)			; lpParam
	{
	msgbox, % A_ThisFunc "() failed with error " DllCall("GetLastError")
	Return 0
	}
idx++
sz := Ceil(10/PtrSz)*PtrSz+2*PtrSz
DllCall("SendMessage" AW
	, Ptr, hwnd
	, "UInt", 0x41E		; TB_BUTTONSTRUCTSIZE
	, "UInt", sz
	, "UInt", 0)
/*
DllCall("SendMessage" AW
	, Ptr, hwnd
	, "UInt", 0x438		; TB_SETSTYLE
	, "UInt", 0
	, "UInt", style)
*/
DllCall("SendMessage" AW
	, Ptr, hwnd
	, "UInt", 0x454		; TB_SETEXTENDEDSTYLE
	, "UInt", 0
	, "UInt", exstyle)
Return hwnd
}
;================================================================
; TBSTATE_CHECKED=0x1		TBSTATE_INDETERMINATE=0x10
; TBSTATE_PRESSED=0x2		TBSTATE_WRAP=0x20
; TBSTATE_ENABLED=0x4		TBSTATE_ELLIPSES=0x40
; TBSTATE_HIDDEN=0x8		TBSTATE_MARKED=0x80
TB_AddBtn(hwnd, txt, cmd, stt, stl, idx, dat="0")
;================================================================
{
Global PtrSz, Ptr, AW
p := Ceil(10/PtrSz)*PtrSz
sz := p+2*PtrSz
VarSetCapacity(TBBUTTON, sz, 0)					; TBBUTTON struct
NumPut(idx, TBBUTTON, 0, "UInt")					; iBitmap
NumPut(cmd, TBBUTTON, 4, "UInt")				; idCommand
NumPut(stt, TBBUTTON, 8, "UChar")				; fsState
NumPut(stl, TBBUTTON, 9, "UChar")				; fsStyle
NumPut(&dat, TBBUTTON, p, Ptr)					; dwData
NumPut(&txt, TBBUTTON, p+PtrSz, Ptr)					; iString
if !DllCall("SendMessage" AW
		, Ptr, hwnd
		, "UInt", (A_IsUnicode ? 0x444 : 0x414)	; TB_ADDBUTTONSW/A
		, "UInt", 1
		, Ptr, &TBBUTTON)
	msgbox, % A_ThisFunc "() failed with error " DllCall("GetLastError")
return (DllCall("SendMessage" AW
			, Ptr, hwnd
			, "UInt", 0x43A					; TB_GETBUTTONSIZE
			, "UInt", 0
			,  "UInt", 0) >> 16) & 0xFFFF	
}
;================================================================
TB_SetIL(hwnd, p, i="0")	; [x] list type [xx...x|] list number (only where required) [xx...x] handle
;================================================================
{
Global Ptr, AW
r=
ofi := A_FormatInteger
SetFormat, Integer, Hex
Loop, Parse, p, %A_Space%, %A_Space%%A_Tab%
	{
	StringLeft, p1, A_LoopField, 1
	StringTrimLeft, p2, A_LoopField, 1
	if p1=D
		r .= ",D" DllCall("SendMessage" AW
					, Ptr, hwnd
					, "UInt", 0x436			; TB_SETDISABLEDIMAGELIST
					, "UInt", 0
					,  "UInt", p2)
	if p1=H
		r .= ",H" DllCall("SendMessage" AW
					, Ptr, hwnd
					, "UInt", 0x434			; TB_SETHOTIMAGELIST
					, "UInt", 0
					,  "UInt", p2)
	if p1=P
		{
		SetFormat, Integer, D
		v := i ? DllCall("SendMessage" AW
					, Ptr, hwnd
					, "UInt", 0x2007			; CCM_SETVERSION
					, "UInt", i
					, "UInt", 0) : 0
		SetFormat, Integer, Hex
		StringSplit, m, p2, |
		r .= ",P" DllCall("SendMessage" AW
					, Ptr, hwnd
					, "UInt", 0x468			; TB_SETPRESSEDIMAGELIST* (Vista+)
					, "UInt", m1
					,  "UInt", m2)
		}
	if p1=I
		{
		SetFormat, Integer, D
		v := i ? DllCall("SendMessage" AW
					, Ptr, hwnd
					, "UInt", 0x2007			; CCM_SETVERSION
					, "UInt", i
					, "UInt", 0) : 0
		SetFormat, Integer, Hex
		StringSplit, m, p2, |
		r .= ",I" DllCall("SendMessage" AW
					, Ptr, hwnd
					, "UInt", 0x430			; TB_SETIMAGELIST
					, "UInt", m1
					,  "UInt", m2)
		}
	}
SetFormat, Integer, %ofi%
return v r
}
;================================================================
TB_Size(hwnd)
;================================================================
{
Global Ptr, AW
DllCall("SendMessage" AW
	, Ptr, hwnd
	, "UInt", 0x421			; TB_AUTOSIZE
	, "UInt", 0
	, "UInt", 0)
}
;================================================================
TB_Get(hwnd, p)	; sWHS=toolbar metrics, bWHRS=button metrics dd=padding
;================================================================
{
Global Ptr, AW
StringSplit, p, p
if p1=s
	{
	VarSetCapacity(SIZE, 8, 0)
	if !DllCall("SendMessage" AW
			, Ptr, hwnd
			, "UInt", 0x453	; TB_GETMAXSIZE
			, "UInt", 0
			, Ptr, &SIZE)
		return 0
	w := NumGet(SIZE, 0, "UInt")
	h := NumGet(SIZE, 4, "UInt")
	return r := (p2="W" ? w : (p2="H" ? h : (p2="S" ? w | h<<16 : 0)))
	}
if p1=b
	{
	r := DllCall("SendMessage" AW
			, Ptr, hwnd
			, "UInt", 0x428	; TB_GETROWS
			, "UInt", 0
			, "UInt", 0)
	s := DllCall("SendMessage" AW
			, Ptr, hwnd
			, "UInt", 0x43A	; TB_GETBUTTONSIZE
			, "UInt", 0
			, "UInt", 0)
	w := s & 0xFFFF
	h := (s >> 16) & 0xFFFF
	return r := (p2="W" ? w : (p2="H" ? h : (p2="R" ? r : (p2="S" ? s : 0))))
	}
if p1=d
	{
	return DllCall("SendMessage" AW
			, Ptr, hwnd
			, "UInt", 0x456	; TB_GETPADDING
			, "UInt", 0
			, "UInt", 0)
	}
}
;================================================================
TB_GetButtonText(hwnd, btn, out=0)				; Win95
;================================================================
{
Global Ptr, AW, A_CharSize
r := DllCall("SendMessage" AW		; for interprocess calls use VirtualAllocEx()/VirtualFreeEx()
		, Ptr, hwnd				; to allocate the buffer (out is a pointer to buffer)
		, "UInt", (A_IsUnicode ? 0x44B : 0x42D)	; TB_GETBUTTONTEXTW/A
		, "UInt", btn
		, Ptr, out)			; when zero, function only returns length (in chars) without ending NULL
return (r+1)*A_CharSize		; account for ending NULL when returning size
}
;================================================================
TB_Set(hwnd, p)
;================================================================
{
Global Ptr, AW
Loop, Parse, p, %A_Space%, %A_Space%%A_Tab%
	{
	StringLeft, p1, A_LoopField, 1
	StringTrimLeft, p2, A_LoopField, 1
	if p1=s
		DllCall("SendMessage" AW
			, Ptr, hwnd
			, "UInt", 0x438	; TB_SETSTYLE
			, "UInt", 0
			, "UInt", p2+0)
	if p1=e
		DllCall("SendMessage" AW
			, Ptr, hwnd
			, "UInt", 0x454	; TB_SETEXTENDEDSTYLE
			, "UInt", 0
			, "UInt", p2+0)
	if p1=b
		DllCall("SendMessage" AW
			, Ptr, hwnd
			, "UInt", 0x41F	; TB_SETBUTTONSIZE
			, "UInt", 0
			, "UInt", p2+0)
	if p1=d
		DllCall("SendMessage" AW
			, Ptr, hwnd
			, "UInt", 0x457	; TB_SETPADDING
			, "UInt", 0
			, "UInt", p2+0)
	}
}
;================================================================
TB_BtnSet(hwnd, idx, p)
;================================================================
{
Global PtrSz, Ptr, AW
dwMask=0
sz := 24+2*PtrSz+4*OSTest("VISTA", ">=")
VarSetCapacity(TBBI, sz, 0)			; TBBUTTONINFO struct
NumPut(sz, TBBI, 0, "UInt")
Loop, Parse, p, |, %A_Space%
	{
	StringLeft, p1, A_LoopField, 1
	StringTrimLeft, p2, A_LoopField, 1
	if p1=c	; idCommand
		{
		dwMask |= 0x20	; TBIF_COMMAND
		NumPut(p2+0, TBBI, 8, "Int")
		}
	if p1=i	; iImage
		{
		dwMask |= 0x1	; TBIF_IMAGE
		NumPut(p2+0, TBBI, 12, "Int")
		}
	if p1=s	; fsState
		{
		dwMask |= 0x4	; TBIF_STATE
; if not intended to be disabled, add ENABLED automatically
		p2 := (!p2 OR p2=8 OR p2=0x10 ? p2 : p2 | 0x4)
		NumPut(p2+0, TBBI, 16, "UChar")
		}
	if p1=y	; fsStyle
		{
		dwMask |= 0x8	; TBIF_STYLE
		NumPut(p2+0, TBBI, 17, "UChar")
		}
	if p1=w	; cx
		{
		dwMask |= 0x40	; TBIF_SIZE
		NumPut(p2+0, TBBI, 18, "UShort")
		}
	if p1=p	; lParam
		{
		dwMask |= 0x10	; TBIF_LPARAM
		NumPut(p2+0, TBBI, 20, Ptr)
		}
	if p1=t	; pszText
		{
		dwMask |= 0x2	; TBIF_TEXT
		NumPut(&p2, TBBI, 20+PtrSz, Ptr)
		}
	if p1=l	; iImageLabel
		{
		dwMask |= 0x80	; TBIF_IMAGELABEL (guessed!!!)
		NumPut(p2+0, TBBI, 24+2*PtrSz, "Int")
		}
	}
NumPut(dwMask, TBBI, 4, "UInt")
if !DllCall("SendMessage" AW
		, Ptr, hwnd
		, "UInt", (A_IsUnicode ? 0x440 : 0x442)	; TB_SETBUTTONINFOW/A
		, "UInt", idx
		, Ptr, &TBBI)
	msgbox, % "Error in " A_ThisFunc "() -> TB_SETBUTTONINFO" AW
}
;================================================================
TB_BtnState(hwnd, idx, p)	; e=enabled c=checked p=pressed h=hidden i=indeterminate l=highlighted
;================================================================
{
Global Ptr, AW
Static c="ecphil"
return DllCall("SendMessage" AW
			, Ptr, hwnd
			, "UInt", (0x408+InStr(c, p))			; TB_ISBUTTON []
			, "UInt", idx
			, "UInt", 0)
}
;================================================================
TB_Cleanup(hTB)
;================================================================
{
Global
return DllCall("DestroyWindow", Ptr, hTB)
}
;================================================================
#include func_GetHwnd.ahk
#include func_OSTest.ahk


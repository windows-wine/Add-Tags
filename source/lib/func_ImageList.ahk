;==VERSIONING==>
;About:	Collection of ImageList functions that allow custom icon sizes
;Compat:	B1.0.48.05,L1.1.13.0
;Date:	2017.01.04 23:35
;Version:	3.3
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;Notes:	� Drugwash, Feb 2012-Jan 2017, Many thanks to SKAN for fixing bitmap stretch issue under XP
;==VERSIONING==<
;================================================================
; default: 16x16 ILC_COLOR24 ILC_MASK
; M=ILC_MASK P=ILC_PERITEMMIRROR I=ILC_MIRROR D=ILC_COLORDDB accepted colors: 4 8 16 24 32
ILC_Create(i, g="1", s="16x16", f="M24")
;================================================================
{
Global Ptr
if i < 0
	return 0
StringSplit, s, s, x
s2 := s2 ? s2 : (s1 := s)
c=
Loop, Parse, f
	if A_LoopField is digit
		c .= A_LoopField
StringReplace, f, f, %c%,,
m := c | (f="M" ? 0x1 : f="P" ? 0xA000 : f="I" ? 0x2000 : f="D" ? 0xFE : "0")
return DllCall("ImageList_Create"
		, "Int", s1
		, "Int", s2
		, "UInt", m
		, "Int", i
		, "Int", g
		, Ptr)
}
;================================================================
ILC_List(cx, file, idx="100", cd="D")	; cd=mask. D=Default (top-left px), N=none, 0xNNNNNN=color
;================================================================
{
Global Ptr, AW
mask := cd="D" ? 0xFF000000 : cd="N" ? 0xFFFFFFFF : cd
Loop, %file%
	if A_LoopFileExt in exe,dll
		{
		if !hInst := DllCall("GetModuleHandle" AW, "Str", file)
			hL := hInst := DllCall("LoadLibrary" AW, "Str", file)
		if idx is not integer
			i := &idx
		else i := idx
		hIL := DllCall("ImageList_LoadImage" AW
				, Ptr, hInst
				, "UInt", i
				, "Int", cx
				, "Int", 1
				, "UInt", mask
				, "UInt", 0
				, "UInt", 0x2000
				, Ptr)
		}
	else if A_LoopFileExt in bmp
		hIL := DllCall("ImageList_LoadImage" AW
				, Ptr, 0
				, "Str", file
				, "Int", cx
				, "Int", 1
				, "UInt", mask
				, "UInt", 0
				, "UInt", 0x2010
				, Ptr)
if (hInst && hL)
	DllCall("FreeLibrary", Ptr, hL)
DllCall("DeleteObject", Ptr, hInst)
return hIL
}
;================================================================
ILC_Overlay(hIL, iIdx, oIdx)	; specify overlay(s) as 1-based indexes: 1,3,12 (max 15 overlays in v4.71)
;================================================================
{
Global Ptr
DllCall("ImageList_SetOverlayImage", Ptr, hIL, "UInt", iIdx-1, "UInt", oIdx)
}
;================================================================
ILC_FitBmp(hPic, hIL, idx="1", tr="0", ovl="")
;================================================================
{
Static
Global Ptr, AW, w9x
;Critical
PtrSz := A_PtrSize ? A_PtrSize : "4"
WinGetPos,,, W1, H1, ahk_id %hPic%
if (W1 && H1)
	W := W1, H := H1
VarSetCapacity(IMAGEINFO, 24+2*PtrSz, 0)
DllCall("ImageList_GetImageInfo"
	, Ptr, hIL
	, "Int", idx-1
	, Ptr, &IMAGEINFO)
bx := NumGet(IMAGEINFO, 8+2*PtrSz, "Int"), by := NumGet(IMAGEINFO, 12+2*PtrSz, "Int")
bw := NumGet(IMAGEINFO, 16+2*PtrSz, "Int")-bx, bh := NumGet(IMAGEINFO, 20+2*PtrSz, "Int")-by
;
;hDC := DllCall("GetDC", Ptr, hPic)
hDC := DllCall("GetDCEx"
		, Ptr, hPic
		, Ptr, 0
		, "UInt", 0x21			; DCX_PARENTCLIP DCX_WINDOW
		, Ptr)
hDC2 := DllCall("CreateCompatibleDC", Ptr, hDC)
hBmp1 := DllCall("CreateCompatibleBitmap" , Ptr, hDC, "Int", 1, "Int", 1)
hBo := DllCall("SelectObject", Ptr, hDC2, Ptr, hBmp1)
DllCall("SetStretchBltMode"
	, Ptr, hDC
	, "UInt", (w9x ? "3" : "4"))	; COLORONCOLOR/STRETCH_HALFTONE
DllCall("SetBrushOrgEx"
	, Ptr, hDC
	, "Int", 0
	, "Int", 0
	, Ptr, 0)
DllCall("BitBlt"
	, Ptr, hDC2
	, "Int", 0
	, "Int", 0
	, "Int", 1
	, "Int", 1
	, Ptr, hDC
	, "Int", 0
	, "Int", 0
	, "UInt", 0x00CC0020)
DllCall("StretchBlt"
	, Ptr, hDC
	, "Int", 0
	, "Int", 0
	, "Int", W
	, "Int", H
	, Ptr, hDC2
	, "Int", 0
	, "Int", 0
	, "Int", 1
	, "Int", 1
	, "UInt", 0x00CC0020)
hB02 := DllCall("SendMessage"
			, Ptr, hPic
			, "UInt", 0x172	; STM_SETIMAGE
			, "UInt", 0		; IMAGE_BITMAP
			, Ptr, hBmp1)
DllCall("DeleteObject", Ptr, hBo2)
DllCall("SelectObject", Ptr, hDC2, Ptr, hBo)
DllCall("DeleteDC", Ptr, hDC2)
;
Sleep, 0
ovl := ovl ? "0," ovl : 1
Loop, Parse, ovl, CSV
	DllCall("ImageList_Draw"
		, Ptr, hIL
		, "UInt", (A_Index="1" ? idx-1 : A_LoopField-1)
		, Ptr, hDC
		, "Int", 0		; x
		, "Int", 0		; y
		, "UInt", tr)	; fStyle (ILD_NORMAL/TRANSPARENT)

hDC2 := DllCall("CreateCompatibleDC", Ptr, hDC)
hBmp := DllCall("CreateCompatibleBitmap" , Ptr, hDC, "Int", W, "Int", H)
hBo := DllCall("SelectObject", Ptr, hDC2, Ptr, hBmp)
/*
DllCall("BitBlt"
	, Ptr, hDC2
	, "Int", 0
	, "Int", 0
	, "Int", W
	, "Int", H
	, Ptr, hDC
	, "Int", 0
	, "Int", 0
	, "UInt", 0x00CC0020)
*/
DllCall("SetStretchBltMode"
	, Ptr, hDC2
	, "UInt", (w9x ? "3" : "4"))	; COLORONCOLOR/STRETCH_HALFTONE
DllCall("SetBrushOrgEx"			; Required in 9x with RP9/ME updates
	, Ptr, hDC2
	, "Int", 0
	, "Int", 0
	, Ptr, 0)
DllCall("StretchBlt"
	, Ptr, hDC2
	, "Int", 0
	, "Int", 0
	, "Int", W
	, "Int", H
	, Ptr, hDC
	, "Int", 0
	, "Int", 0
	, "Int", bw
	, "Int", bh
	, "UInt", 0x00CC0020)

DllCall("SelectObject", Ptr, hDC2, Ptr, hBo)
DllCall("DeleteDC", Ptr, hDC2)
DllCall("ReleaseDC", Ptr, hPic, Ptr, hDC)
Sleep, 0
if hP := DllCall("SendMessage" AW
			, Ptr, hPic
			, "UInt", 0x172	; STM_SETIMAGE
			, "UInt", 0		; IMAGE_BITMAP
			, Ptr, hBmp)
	DllCall("DeleteObject", Ptr, hP)
return hBmp
}
;================================================================
ILC_FitBmp2(hPic, hIL, idx="1", tr="0", ovl="")
;================================================================
{
Static
Global Ptr, AW
PtrSz := A_PtrSize ? A_PtrSize : "4"
WinGetPos,,, W1, H1, ahk_id %hPic%
if (W1 && H1)
	W := W1, H := H1
VarSetCapacity(IMAGEINFO, 24+2*PtrSz, 0)
DllCall("ImageList_GetImageInfo"
	, Ptr, hIL
	, "Int", idx-1
	, Ptr, &IMAGEINFO)
bx := NumGet(IMAGEINFO, 8+2*PtrSz, "Int"), by := NumGet(IMAGEINFO, 12+2*PtrSz, "Int")
bw := NumGet(IMAGEINFO, 16+2*PtrSz, "Int")-bx, bh := NumGet(IMAGEINFO, 20+2*PtrSz, "Int")-by
hDC := DllCall("CreateCompatibleDC", Ptr, 0)
hBm1 := DllCall("CreateBitmap"
		, "Int", bw
		, "Int", bh
		, "UInt", 1
		, "UInt", 0x18
		, "UInt", 0
		, Ptr)
hBmp2 := DllCall("CopyImage"
		, Ptr, hBm1
		, "UInt", 0		; 0=IMAGE_BITMAP
		, "Int", 0
		, "Int", 0
		, "UInt", 0x2008
		, Ptr)
hBold := DllCall("SelectObject", Ptr, hDC, Ptr, hBmp2)
tr += 0x10 + ovl<<8
DllCall("ImageList_Draw"
	, Ptr, hIL
	, "Int", idx-1
	, Ptr, hDC
	, "Int", 0
	, "Int", 0
	, "UInt", tr)			; ILD_NORMAL/TRANSPARENT
DllCall("DeleteObject", Ptr, hBm1)
DllCall("SelectObject", Ptr, hDC, Ptr, hBold)
DllCall("DeleteDC", Ptr, hDC)
hBmp := DllCall("CopyImage"
		, Ptr, hBmp2
		, "UInt", 0		; 0=IMAGE_BITMAP
		, "Int", W
		, "Int", H
		, "UInt", 0x2008
		, Ptr)
; according to control class, use BM_SETIMAGE or STM_SETIMAGE, IMAGE_BITMAP
WinGetClass, cls, ahk_id %hPic%
if hP := DllCall("SendMessage" AW
		, Ptr, hPic
		, "UInt", (cls="Button" ? 0xF7 : 0x172)
		, "UInt", 0
		, Ptr, hBmp
		, Ptr)
	DllCall("DeleteObject", Ptr, hP)
DllCall("DeleteObject", Ptr, hBmp2)
return hBmp
}
;================================================================
ILC_SetBmpTxt(hPic, hIL, idx="1", tr="0", txt="", pos="", attr="")
;================================================================
{
Static
Global Ptr, AW, w9x
PtrSz := A_PtrSize ? A_PtrSize : "4"
dte=DrawTextEx		; Define function name here so we can call it dynamically
WinGetPos,,, W1, H1, ahk_id %hPic%
if (W1 && H1)
	W := W1, H := H1
VarSetCapacity(IMAGEINFO, 24+2*PtrSz, 0)
DllCall("ImageList_GetImageInfo"
	, Ptr, hIL
	, "Int", idx-1
	, Ptr, &IMAGEINFO)
bx := NumGet(IMAGEINFO, 8+2*PtrSz, "Int"), by := NumGet(IMAGEINFO, 12+2*PtrSz, "Int")
bw := NumGet(IMAGEINFO, 16+2*PtrSz, "Int")-bx, bh := NumGet(IMAGEINFO, 20+2*PtrSz, "Int")-by
SetFormat, Integer, H
hIL+=0
hD := DllCall("GetDC", Ptr, hPic)
DllCall("ImageList_Draw"
	, Ptr, hIL
	, "Int", idx-1
	, Ptr, hD
	, "Int", 0		; x
	, "Int", 0		; y
	, "UInt", tr)	; fStyle (ILD_NORMAL/TRANSPARENT)
hDC := DllCall("CreateCompatibleDC", Ptr, hD)
hBmp := DllCall("CreateCompatibleBitmap"
		, Ptr, hD
		, "Int", bw		; nWidth
		, "Int", bh		; nHeight
		, Ptr)
hBold := DllCall("SelectObject", Ptr, hDC, Ptr, hBmp)
SetFormat, Integer, D
DllCall("SetStretchBltMode", Ptr, hDC, "UInt", (w9x ? "3" : "4"))
DllCall("SetBrushOrgEx", Ptr, hDC, "Int", 0, "Int", 0, Ptr, 0)
DllCall("StretchBlt"
	, Ptr, hDC
	, "Int", 0
	, "Int", 0
	, "Int", W
	, "Int", H
	, Ptr, hD
	, "Int", 0
	, "Int", 0
	, "Int", bw
	, "Int", bh
	, "UInt", 0x00CC0020)
if txt && IsFunc(dte)
	%dte%(hDC, txt, pos, attr)
DllCall("SelectObject", Ptr, hDC, Ptr, hBold)
DllCall("ReleaseDC", Ptr, hPic, Ptr, hD)
DllCall("DeleteDC", Ptr, hDC)
if hP := DllCall("SendMessage" AW
		, Ptr, hPic
		, "UInt", 0x172
		, "UInt", 0
		, Ptr, hBmp
		, Ptr)
	DllCall("DeleteObject", Ptr, hP)
return hBmp
}
;================================================================
ILC_FitBmp3(hPic, hIL, idx="1")
;================================================================
{
Global Ptr, AW
WinGetPos,,, W1, H1, ahk_id %hPic%
if (W1 && H1)
	{
	W := W1
	H := H1
	}
hBmp := ILC_ImageResize(hIL, idx, W "x" H)
hP := DllCall("SendMessage" AW
			, Ptr, hPic
			, "UInt", 0x172	; STM_SETIMAGE
			, "UInt", 0		; IMAGE_BITMAP
			, Ptr, hBmp
			, Ptr)
return hBmp	; requires cleanup on exit!
}
;================================================================
ILC_ImageResize(hIL, idx, sz="")
;================================================================
{
Global Ptr, PtrP
ofi := A_FormatInteger
SetFormat, Integer, H
DllCall("ImageList_GetIconSize", Ptr, hIL, PtrP, bw, PtrP, bh)
StringSplit, s, sz, x
s1 := s1 ? s1 : bw
s2 := s2 ? s2 : bh
hDC := DllCall("CreateCompatibleDC", Ptr, 0)
hBmp := DllCall("CreateBitmap"
		, "Int", bw
		, "Int", bh
		, "UInt", 1	; 1color plane
		, "UInt", 0x18	; 24bit
		, "UInt", 0
		, Ptr)
hBmp2 := DllCall("CopyImage"
		, Ptr, hBmp
		, "UInt", 0	; 0=IMAGE_BITMAP
		, "Int", 0
		, "Int", 0
		, "UInt", 0x2004
		, Ptr)
DllCall("DeleteObject", Ptr, hBmp)
hBo := DllCall("SelectObject", Ptr, hDC, Ptr, hBmp2)
DllCall("ImageList_Draw"
	, Ptr, hIL
	, "Int", idx-1
	, Ptr, hDC
	, "Int", 0
	, "Int", 0
	, "UInt", 0) 		; ILD_NORMAL
DllCall("SelectObject", Ptr, hDC, Ptr, hBo)
DllCall("DeleteDC", Ptr, hDC)
hBmp := ResizeBmp(hBmp2, s1, s2)
DllCall("DeleteObject", Ptr, hBmp2)
SetFormat, Integer, %ofi%
return hBmp
}
;================================================================
ResizeBmp(hBmp, w, h="")
;================================================================
{
Global
h := h ? h : w
if hBmp
	return DllCall("CopyImage"
			, Ptr, hBmp
			, "UInt", 0		; 0=IMAGE_BITMAP
			, "Int", w
			, "Int", h
			, "UInt", 0x2004	; LR_CREATEDIBSECTION LR_COPYRETURNORG
			, Ptr)
; requires cleanup on exit!
}
;================================================================
; sz=individual image width, file=file path or ImageList handle
; res=resource index in file, cd=color depth (1-32bit, 0-lower)
GetBmp(idx, sz, ByRef file, res="", cd="D")
;================================================================
{
if file is integer
	hIL := file
else IfExist, %file%
	{
	if !hIL := ILC_List(sz, file, res, cd)
		return 0
	file := hIL
	}
else return 0
ofi := A_FormatInteger
SetFormat, Integer, H
file+=0
hIL+=0
SetFormat, Integer, %ofi%
return ILC_ImageResize(hIL, idx, sz)
}
;================================================================
GetPixelColor(hBmp, px, py)
;================================================================
{
Global Ptr
ofi := A_FormatInteger
SetFormat, Integer, H
hDC := DllCall("CreateCompatibleDC", Ptr, 0)
hBo := DllCall("SelectObject", Ptr, hDC, Ptr, hBmp)
sz := DllCall("GetPixel", Ptr, hDC, "Int", px, "Int", py, Ptr)
DllCall("SelectObject", Ptr, hDC, Ptr, hBo)
DllCall("DeleteDC", Ptr, hDC)
SetFormat, Integer, %ofi%
return sz
}
;================================================================
GetBmpInfo(file, ByRef bw, ByRef bh, ByRef bpp)
;================================================================
{
Global Ptr, AW
if InStr(file, "|")
	{
	StringSplit, f, file, |
	file := f1, idx := f2
	}
Loop, %file%
	if A_LoopFileExt in exe,dll
		{
		if (idx="")
			return 0
		if !hInst := DllCall("GetModuleHandle" AW, "Str", file)
			hL := hInst := DllCall("LoadLibraryEx" AW, "Str", file, "UInt", 0, "UInt", 0x2)
			; LOAD_LIBRARY_AS_DATAFILE. Don't use DONT_RESOLVE_DLL_REFERENCES !!!
		; need to use MAKEINTRESOURCE here
		if idx is not integer
			i := &idx
		else i := idx
		hBmp := DllCall("LoadImage" AW
					, Ptr, hInst
					, "UInt", i
					, "UInt", 0
					, "Int", 0
					, "Int", 0
					, "UInt", 0x2000
					, Ptr)
		}
	else if A_LoopFileExt in bmp
		hBmp := DllCall("LoadImage" AW
					, Ptr, 0
					, "Str", file
					, "UInt", 0
					, "Int", 0
					, "Int", 0
					, "UInt", 0x2010
					, Ptr)
if hL
	DllCall("FreeLibrary", Ptr, hL)
if !hBmp
	return 0
VarSetCapacity(buf, sz := 24, 0)	; BITMAP struct (DIBSECTION is 84, not needed)
DllCall("GetObject", Ptr, hBmp, "UInt", sz, Ptr, &buf)
bw := NumGet(buf, 4, "Int")
bh := NumGet(buf, 8, "Int")
bpp := NumGet(buf, 18, "UShort")
DllCall("DeleteObject", Ptr, hBmp)
DllCall("DeleteObject", Ptr, hInst)
VarSetCapacity(buf, 0)			; Free buffer memory
}
;================================================================
SetBmp(hDest, hBmp)
;================================================================
{
Global
return DllCall("SendMessage" AW
		, Ptr, hDest
		, "UInt", 0x172	; STM_SETIMAGE
		, "UInt", 0		; IMAGE_BITMAP
		, Ptr, hBmp)
}
;================================================================
ILC_Count(hwnd)
;================================================================
{
Global
if hwnd
	return DllCall("ImageList_GetImageCount", Ptr, hwnd)
}
;================================================================
ILC_Destroy(hwnd)
;================================================================
{
Global
return DllCall("ImageList_Destroy", Ptr, hwnd)
}
;================================================================
; LR_CREATEDIBSECTION=0x2000 LR_LOADFROMFILE=0x10 LR_LOADTRANSPARENT=0x20 LR_SHARED=0x8000
; IMAGE_BITMAP=0x0 IMAGE_ICON=0x1 IMAGE_CURSOR=0x2
ILC_Add(hIL, icon, idx="1", mask="")
;================================================================
{
Global Ptr, AW
Static it="BIC"
StringLeft, t, icon, 1
StringTrimLeft, icon, icon, 1
t := InStr(it, t)-1
if (t<0 OR hIL=0)	; this one had it broken forever !!!
	return 0
hInst=0
mask := (mask & 0xFFFFFFFF)!="" ? mask : ""
if icon is integer
	hIcon := icon
else
	{
	Loop, %icon%
		if A_LoopFileExt in exe,dll
			{
			if !hInst := DllCall("GetModuleHandle" AW, "Str", icon)
;				hL := hInst := DllCall("LoadLibrary" AW, "Str", icon)
				hL := hInst := DllCall("LoadLibraryEx" AW, "Str", icon, "UInt", 0, "UInt", 0x2, Ptr)
				; LOAD_LIBRARY_AS_DATAFILE. Don't use DONT_RESOLVE_DLL_REFERENCES !!!
			flags=0x2000
		; need to use MAKEINTRESOURCE here
			if idx is not integer
				i := &idx
			else i := idx
			hIcon := DllCall("LoadImage" AW
						, Ptr, hInst
						, "UInt", i
						, "UInt", t
						, "Int", 0
						, "Int", 0
						, "UInt", flags
						, Ptr)
			}
		else if A_LoopFileExt in bmp,ico,cur,ani
			{
			flags=0x2010
			hIcon := DllCall("LoadImage" AW
						, Ptr, hInst
						, "Str", icon
						, "UInt", t
						, "Int", 0
						, "Int", 0
						, "UInt", flags
						, Ptr)
			}
		else
			{
			i := idx
			flags=0x8000
			hIcon := DllCall("LoadImage" AW
						, Ptr, -1
						, "UInt", i
						, "UInt", t
						, "Int", 0
						, "Int", 0
						, "UInt", flags
						, Ptr)
			}
	}
if (hInst && hL)
	DllCall("FreeLibrary", Ptr, hInst)
if t=0
	{
	if (mask != "")
		r := DllCall("ImageList_AddMasked", Ptr, hIL, Ptr, hIcon, "UInt", mask)
	else r := DllCall("ImageList_Add", Ptr, hIL, Ptr, hIcon, "UInt", 0)
	DllCall("DeleteObject", Ptr, hIcon)
	}
if t=1
	{
	r := DllCall("ImageList_ReplaceIcon", Ptr, hIL, "UInt", -1, Ptr, hIcon)
	DllCall("DestroyIcon", Ptr, hIcon)
	}
if t=2
	{
	r := DllCall("ImageList_ReplaceIcon", Ptr, hIL, "UInt", -1, Ptr, hIcon)
	DllCall("DestroyCursor", Ptr, hIcon)
	}
DllCall("DeleteObject", Ptr, hInst)
return r
}
;================================================================
IL_AddIcon(hIL, hIcon, idx=-1)
;================================================================
{
Global Ptr
; return DllCall("comctl32\ImageList_AddIcon", Ptr, hIL, Ptr, hIcon)
return DllCall("ImageList_ReplaceIcon", Ptr, hIL, "UInt", idx, Ptr, hIcon)
}
;================================================================
ILC_GetIcon(hIL, idx, f=0x1)
;================================================================
{
Global Ptr
if f < 0x10		; ILD_NORMAL/TRANSPARENT/FOCUS/BLEND/MASK (default: transparent)
	return DllCall("ImageList_GetIcon", Ptr, hIL, "Int", idx-1, "UInt", f)
return 0
}
;================================================================
IL_ReplaceColor(hIL, idx, color, b="")	; WHITE/LTGRAY/GRAY/DKGRAY/BLACK/NULL_BRUSH
;================================================================
{
Global Ptr, PtrP, AW
Static bord="WLGDBN"
StringSplit, b, b
DllCall("ImageList_GetIconSize", Ptr, hIL, PtrP, w, PtrP, h)
hDC0 := DllCall("GetDC", Ptr, 0)
hDC := DllCall("CreateCompatibleDC", Ptr, hDC0)
hBmp := DllCall("CreateCompatibleBitmap" , Ptr, hDC0, "Int", w, "Int", h)
hBr := DllCall("CreateSolidBrush", "UInt", color)
oBmp := DllCall("SelectObject", Ptr, hDC, Ptr, hBmp)
VarSetCapacity(RECT, 16, 0)
NumPut(w, RECT, 8, "UInt")
NumPut(h, RECT, 12, "UInt")
DllCall("FillRect", Ptr, hDC, Ptr, &RECT, Ptr, hBr)
if b1
	{
	NumPut(w, RECT, 8, "UInt")
	NumPut(h, RECT, 12, "UInt")
	DllCall("FrameRect"
		, Ptr, hDC
		, Ptr, &RECT
		, Ptr, DllCall("GetStockObject", "Int", InStr(bord, b1)-1))	; outer border
	}
if b2
	{
	NumPut(1, RECT, 0, "UInt")
	NumPut(1, RECT, 4, "UInt")
	NumPut(w-1, RECT, 8, "UInt")
	NumPut(h-1, RECT, 12, "UInt")
	DllCall("FrameRect"
		, Ptr, hDC
		, Ptr, &RECT
		, Ptr, DllCall("GetStockObject", "Int", InStr(bord, b2)-1))	; inner border
	}
DllCall("SelectObject", Ptr, hDC, Ptr, oBmp)
r := DllCall("ImageList_Replace", Ptr, hIL, "Int", idx-1, Ptr, hBmp, Ptr, 0)
DllCall("DeleteObject", Ptr, hBmp)
DllCall("DeleteDC", Ptr, hDC)
DllCall("ReleaseDC", Ptr, 0, Ptr, hDC0)
return r
}

;================================================================
; we need to add ImageList_AddMasked() to work with Magenta transparency (for RP9 and others)
#include *i func_DrawTextEx.ahk
